from whiteboard.drawable import Drawable, drawable_registry, Callable
import sets

class Text(Drawable):
    def __init__(self):
	Drawable.__init__(self, 'text')
	self._x = 0.0
	self._y = 0.0
	self._msg = ''

    # Implement __deepcopy__, see xchat/README item 3b.
    def __deepcopy__(self, memo):
        new = Text()
        new.deepcopy_from(self, memo)
        return new
        
    def deepcopy_from (self, old, memo):
        Drawable.deepcopy_from(self, old, memo)
        self._msg = old._msg
        self._x = old._x
        self._y = old._y

    def set(self, msg, x, y):
        self._msg = msg
	self._x = x
	self._y = y

    def set_text(self, text):
	self._msg = text

    def get_text(self):
	return self._msg

    def set_x(self, x):
	self._x = x

    def get_x(self):
	return self._x

    def set_y(self, y):
	self._y = y

    def get_y(self):
	return self._y

    def serialize_attribute(attr_name, attr_value):
        assert (attr_name in ['x', 'y', 'text'])
        return str(attr_value)

    def deserialize_attribute(attr_name, attr_value):
        assert (attr_name in ['x', 'y', 'text'])
        return attr_value

    # Stupid hack to allow static "class method"
    serialize_attribute = Callable(serialize_attribute)
    deserialize_attribute = Callable(deserialize_attribute)
    
    def load_attributes(self, attrs):
	self.set(attrs['text'], float(attrs['x']), float(attrs['y']))

    def get_attributes(self):
	return {'text': self._msg, 'x': `float(self._x)`, 'y': `float(self._y)` }

    def modify_msg(self):
	return Drawable.modify_msg(self, self.attributes())

drawable_registry.register('text', sets.Set(['text', 'x', 'y']), Text)

if __name__ == '__main__':
    t = Text('', 0, 10)
    print t.create_msg()
    print t.modify_msg()
    t.set_text('hi')
    print t.modify_msg()
    t.set_x(100)
    print t.modify_msg()

