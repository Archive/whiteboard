import re

class ClientProtocolError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

# From supybot source code, Copyright (c) 2002, Jeremiah Fincher
def isChannel(s):
    """Returns True if s is a valid IRC channel name."""
    return (s and s[0] in '#&+!' and len(s) <= 50 and \
            '\x07' not in s and ',' not in s and ' ' not in s)

class ClientParser:
    def __init__(self):
        self.message_fragment = {}
        self.message_seq = {}
        
    def get_channel(self, recipient, message):
        m = re.match("^WHITEBOARD (([^0-9 ]+) )?([0-9]+\+? .*)", message)
        if not m:
            raise ClientProtocolError("Invalid fragment: \"" + message + "\"")

        if m.group(1):
            chname = m.group(2)
            if isChannel(recipient):
                raise ClientProtocolError("channel cannot be specified in command sent to a channel")
            if not isChannel(chname):
                raise ClientProtocolError("'%s' is not a valid channel name" % chname)
            return chname
        else:
            if not isChannel(recipient):
                raise ClientProtocolError("channel must be specified in private command")
            return recipient
            
    def parse_message(self, nick, message):
        # Rip off the whiteboard header, and parse out the parts that we care about.
        m = re.match("^WHITEBOARD (([^0-9 ]+) )?([0-9]+)(\+?) (.+$)", message)
        if not m:
            raise ClientProtocolError("Invalid fragment: \"" + message + "\"")
            return
    
        serial = int(m.group(3))
        continuing = m.group(4)
        fragment = m.group(5)

        if continuing == '+':
            is_fragment = 1
        else:
            is_fragment = 0

        return self.process_fragment(fragment, serial, is_fragment, nick)

    def process_fragment(self, fragment, serial, is_fragment, nick):
        # This is either not a fragment, or is the end of a message
        if not is_fragment:
            if nick not in self.message_fragment:
                if serial != 0:
                    raise ClientProtocolError("message fragment %s received out of sequence" % serial)
                self.end_message(nick)
                return fragment

            # make sure this followed the protocol
            if self.message_seq[nick] != (serial-1):
                raise ClientProtocolError("final serial %s received out of sequence" % serial)
            self.message_fragment[nick] += fragment
            fragment = self.message_fragment[nick]
            self.end_message(nick)
            return fragment

        # This is a message fragment
        if nick not in self.message_fragment:
            # This is the fist message fragment, check the protocol
            if serial != 0:
                raise ClientProtocolError("first message from nick didn't start with zero")
            self.message_seq[nick] = serial
            self.message_fragment[nick] = fragment
        else:
            # This is not the first message fragment, check the protocol
            # and save it for later.
            if nick not in self.message_seq:
                raise ClientProtocolError("message serial %s seen without zero to start message" % serial)
            if self.message_seq[nick] != (serial-1):
                raise ClientProtocolError("out of sequence message %s" % serial )
            self.message_seq[nick] = serial
            self.message_fragment[nick] += fragment

    # Clear out the state for this nick
    def end_message(self, nick):
        if nick in self.message_seq:
            del self.message_seq[nick]
        if nick in self.message_fragment:
            del self.message_fragment[nick]

def assert_throws(cp, nick, msg):
    e = None
    try:
        cp.parse_message(nick, msg)
    except ClientProtocolError, e:
        cp.end_message(nick)
    assert e
            
if __name__ == '__main__':
    cp = ClientParser()
    assert cp.parse_message("nick1", "WHITEBOARD 0 abc") == "abc"
    
    assert cp.parse_message("nick1", "WHITEBOARD 0+ abc") == None
    assert cp.parse_message("nick1", "WHITEBOARD 1 efg") == "abcefg"

    assert cp.parse_message("nick1", "WHITEBOARD 0+ abc ") == None
    assert cp.parse_message("nick1", "WHITEBOARD 1 efg") == "abc efg"

    assert cp.parse_message("nick1", "WHITEBOARD 0+ abc") == None
    assert cp.parse_message("nick2", "WHITEBOARD 0+ abc") == None
    assert cp.parse_message("nick1", "WHITEBOARD 1 efg") == "abcefg"
    assert cp.parse_message("nick2", "WHITEBOARD 1 efg") == "abcefg"

    assert_throws(cp, "nick1", "WBOARD 0+ abc")
    
    assert_throws(cp, "nick1", "WHITEBOARD 1 abc")
    
    cp.parse_message("nick1", "WHITEBOARD 0+ abc")
    assert_throws(cp, "nick1", "WHITEBOARD 2 abc")
