from whiteboard.actions import Create, Modify, SubtractPoints, AppendPoints, Delete
import gobject
from copy import deepcopy
import random, time, sys

class ChangeSet:
    def __init__(self, changed_objects):
        self._changed_objects = changed_objects

    def get_changed_objects(self):
        return self._changed_objects


class Model(gobject.GObject):

    __gsignals__ = {
        'changed' : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, (gobject.TYPE_PYOBJECT,))
        }

    def __init__(self, dispatcher):
        gobject.GObject.__init__(self)

        self._dispatcher = dispatcher
        dispatcher.connect("add_actions", self._dispatcher_add_actions_cb)
        
        self._objects = self._fetch_initial_objects()
        self._pending_actions = [ ]
        
        self._in_transaction = False
        self._untransmitted_actions = None
        
    def add_actions(self, actions):
        """Transmits a list of requested actions to the master client,
        and adds them to our set of pending actions. The requestId on
        the actions will also be set"""

        for action in actions:
            self._untransmitted_actions.append(action)
            action.set_request_id(self._generate_request_id())

        self._append_to_pending_actions(actions)

        if not self._in_transaction:
            self.commit_transaction()

    def begin_transaction(self):
        assert(not self._in_transaction)
        assert(self._untransmitted_actions == None)
        self._in_transaction = True
        self._untransmitted_actions = [ ]

    def rollback_transaction(self):
        # Remove the untransmitted actions from the pending actions
        for action in self._untransmitted_actions:
            self._pending_actions.remove(action)            
        self._untransmitted_actions = None
        self._in_transaction = False
        
        # FIXME: only announce that objects affected by these actions
        # changed, not every single objects
        self._emit_changed_signal(self.get_objects().values())
        
    def commit_transaction(self):
        (compressed_actions, dropped_actions) = self._compress_action_stream(self._untransmitted_actions)
        for action in dropped_actions:
            self._pending_actions.remove(action)
        self._untransmitted_actions = None
        self._in_transaction = False
        self._dispatcher.dispatch_actions(compressed_actions)
        
    # Viewer API
    def get_objects(self):
        """Returns the current state of the object tree, including pending_actions
        (i.e. actions that have not been officially approved by the master client.
        Return value is a dict from objectIds to the objects"""
        # FIXME: we really should cache the execution of pending_actions
        objects = deepcopy(self._objects)
        self._run_actions(self._pending_actions, objects)
        return objects

    def _compress_action_stream(self, actions):
        """Returns a shorter list of actions that will produce an equivalent effect
        to the list of actions passed in (including producing the same requestId
        and hence objectId for Create actions)"""
        return self._compress_contiguous_append_points(actions)

    def _compress_contiguous_append_points(self, actions):
        compressed_actions = [ ]
        dropped_actions = [ ]
        base_append_action = None
        for action in actions:
            if isinstance(action, AppendPoints):
                if base_append_action and (action.get_object_id() == base_append_action.get_object_id()):
                    dropped_actions.append(action)
                    base_append_action.append_more_points(action.get_points())
                else:
                    compressed_actions.append(action)
                    base_append_action = action
            else:
                compressed_actions.append(action)
                base_append_action = None
        return (compressed_actions, dropped_actions)
                
    def _dispatcher_add_actions_cb(self, dispatcher, actions, authoritative):
        if authoritative:
            self._commit_actions(actions)
        else:
            self._append_to_pending_actions(actions)

    def _append_to_pending_actions(self, actions):
        # Change a copy of the object tree
        changed_objects = self._run_actions(actions, self.get_objects())

        for action in actions:
            self._pending_actions.append(action)

        self._emit_changed_signal(changed_objects)

    def _commit_actions(self, actions):
        # change the official object tree
        changed_objects = self._run_actions(actions, self._objects)

        # Drop any pending_actions with the same requestId
        for action in actions:
            for pending_action in self._pending_actions:
                if action.get_request_id() == pending_action.get_request_id():
                    self._pending_actions.remove(pending_action)

        self._emit_changed_signal(changed_objects)

    def _run_actions(self, actions, objects):
        changed_objects = [ ]
        
        for action in actions:
            changed_objects = changed_objects + action.run(objects)

        return changed_objects

    def _fetch_initial_objects(self):
        objects = { }
        actions = self._dispatcher.get_initialization_actions()
        self._run_actions(actions, objects)
        return objects

    def _generate_request_id(self):
	return "%s" % (random.randint(0, sys.maxint))

    def _emit_changed_signal(self, changed_objects):
        changeset = ChangeSet(changed_objects)
        self.emit("changed", changeset)

gobject.type_register(Model)

def _test_model_changed_cb(model, changeset):
    for changed in changeset.get_changed_objects():
        print ("'%s' changed" % (str(changed)))
        
def get_test_model():
    from dispatcher import get_test_dispatcher    
    dispatcher = get_test_dispatcher()
    model = Model(dispatcher)
    model.connect("changed", _test_model_changed_cb)
    return model

if __name__ == '__main__':
    from actions import get_test_actions
    actions = get_test_actions()

    model = get_test_model()

    model.add_actions(actions)
