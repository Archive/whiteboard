from whiteboard.transports.transport import FDTransport
import socket, os

class FifoTransport(FDTransport):
    def __init__(self, data):
	FDTransport.__init__(self)
	self._path = data
	FDTransport._set_fd(self, os.open(data, os.O_RDWR))
	
def init(data):
    return FifoTransport(data)

