import gtk

from whiteboard.transports.transport import Transport

class EchoTransport(Transport):
    def __init__(self, response_latency):
        Transport.__init__(self)
        self._response_latency = response_latency
        
    def write(self, data, private=False):
        request = data[len("<request>"):len(data) - len("</request>")]
        print "[Received]: %s" % (request)
        gtk.timeout_add(self._response_latency, self._send_response, request)

    def _send_response(self, request):
        response = "<exec>%s</exec>" % (request)
        print "[Responding]: %s" % (response)
        self.emit("read", response)
        return False

def init(data):
    tp = EchoTransport(response_latency=1000)
    return tp
