import gtk
import gobject
import os
from whiteboard.transports.modload import load

class TransportFactory:
    def make(self, data):
	(name, data) = data.split(':', 1)
	fullname = name + '_transport'
	return load(fullname, data)

class Transport(gobject.GObject):
    __gsignals__ = {
        'read' : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, (gobject.TYPE_PYOBJECT,))
        }
    
    def __init__(self):
        gobject.GObject.__init__(self)

    def write(self, data, private=False):
	pass

class FDTransport(Transport):
    def __init__(self, fd=None):
	if not fd is None:
	    self._set_fd(fd)

    def _set_fd(self, fd):
	self._fd = fd
	gtk.input_add(self._fd, gobject.IO_IN | gobject.IO_ERR | gobject.IO_HUP, self._data_avail)

    def _data_avail(self, sock, condition):
	if condition == gobject.IO_IN:
	    self.emit('read', os.read(self._fd, 4096))
	else:
	    raise Exception("Couldn't read from file descriptor %d" % (fd,))

    def write(self, data, private=False):
	os.write(self._fd, data)

factory = TransportFactory()

gobject.type_register(Transport)
	

