from whiteboard.transports.socket_transport import SocketTransport
import socket, os
import gtk, gobject

class UnixTransport(SocketTransport):
    def __init__(self, data):
	SocketTransport.__init__(self, 'unix', socket.AF_UNIX, data)

    def parse_address(self, addr):
	return addr

def init(data):
    tp = UnixTransport(data)
    tp.start()
    return tp

