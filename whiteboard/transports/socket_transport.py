from whiteboard.transports.transport import Transport
import socket, os
import threading
import gtk, gobject

class SocketTransport(Transport, threading.Thread):
    def __init__(self, name, socktype, data):
	Transport.__init__(self)
	threading.Thread.__init__(self)
	self._lock = threading.Lock()
	sock = socket.socket(socktype, socket.SOCK_STREAM, 0)
	self._outbuf = ''
	self._inwatch = 0
	self._outwatch = 0
	self._errwatch = 0
	(is_server, path) = data.split(':', 1)
	if is_server == 'server':
	    is_server = True
	elif is_server == 'client':
	    is_server = False
	else:
	    print "unrecognized mode; known modes: server, client"
	    sys.exit(1)
	self._path = self.parse_address(path)
	if is_server:
	    self.mode = 'server'
	    self.state = 'accept'
	    print "listening on %s:%s"  % (name, self._path,)
	    sock.bind(self._path)
	    self._conn = None
	else:
	    self.mode = 'client'
	    self.state = None
	    print "connecting to %s:%s"  % (name, self._path,)
	    sock.connect(self._path)
	    self._conn = sock
	    self._watch_conn()
        self.sock = sock

    def run(self):
	if self.mode == 'server':
	    print "listening for connections"
	    self.sock.listen(1)
	    (conn, addr) = self.sock.accept()
	    print "connection from %s"  % (addr,)
	    self._lock.acquire()
	    self._conn = conn
	    self._watch_conn()
	    self._lock.release()

    def _watch_conn(self):
	print "watching fd %d" % (self._conn.fileno(),)
	self._conn.setblocking(0)
	self._inwatch = gtk.input_add(self._conn.fileno(), gobject.IO_IN, self._io_in)
	self._watch_out()
	self._errwatch = gtk.input_add(self._conn.fileno(), gobject.IO_ERR | gobject.IO_HUP, self._io_err)

    def _unwatch_conn(self):
	print "unwatching fd %d" % (self._conn.fileno(),)
	gtk.input_remove(self._inwatch)
	if self._outwatch > 0:
	    gtk.input_remove(self._outwatch)
	gtk.input_remove(self._errwatch)

    def _watch_out(self):
	if self._outbuf != '' and self._outwatch == 0:
	    self._outwatch = gtk.input_add(self._conn.fileno(), gobject.IO_OUT, self._io_out)

    def _unwatch_out(self):	    
	if self._outbuf == '' and self._outwatch > 0:
	    gtk.input_remove(self._outwatch)
	    self._outwatch = 0

    def _io_in(self, fd, condition):
	print "input avail"
	self.emit('read', self._conn.recv(4096))
        return 1

    def _io_out(self, fd, condition):
	print "output avail"
	self._conn.send(self._outbuf)
        return 0

    def _io_err(self, fd, condition):
	self._unwatch_conn()
        self._conn.shutdown(2)
	self._conn = None
	print "Couldn't read from file descriptor %d, terminating connection" % (fd,)
	return None

    def write(self, data, private=False):
	self._lock.acquire()
	self._outbuf += data
	if not self._conn:
	    print "no connection!"
	    self._lock.release()
	    return
	    
	self._watch_out()
	self._outbuf = self._outbuf[self._conn.send(self._outbuf):]
	self._unwatch_out()
	self._lock.release()
