from whiteboard.transports.socket_transport import SocketTransport
import socket, os
import gtk, gobject

class TCPTransport(SocketTransport):
    def __init__(self, data):
	SocketTransport.__init__(self, 'tcp', socket.AF_INET, data)

    def parse_address(self, addr):
	(host, port) = addr.split(':', 1)
	return (host, int(port))

def init(data):
    tp = TCPTransport(data)
    tp.start()
    return tp

