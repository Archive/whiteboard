#!/usr/bin/python

import os
import gtk
import gtk.glade
import cairo
import cairo.gtk
from config import *

from whiteboard.model import Model
from whiteboard.stroke import Point, Stroke
from whiteboard.text import Text
from whiteboard.actions import Create, AppendPoints

class Display(gtk.DrawingArea):

    def __init__ (self, model):
        gtk.DrawingArea.__init__ (self)
	self._mode = 'stroke'
        self.model = model
        self.connect ('expose_event', self.expose_event)
        self.connect ('motion_notify_event', self.motion_notify)
        self.connect ('button_press_event', self.button_press)
        self.connect ('button_release_event', self.button_release)
        self.model.connect ('changed', self.model_changed)
	self.set_events(gtk.gdk.EXPOSURE_MASK |
                        gtk.gdk.LEAVE_NOTIFY_MASK |
                        gtk.gdk.BUTTON_PRESS_MASK |
                        gtk.gdk.BUTTON_RELEASE_MASK |
                        gtk.gdk.POINTER_MOTION_MASK |
                        gtk.gdk.POINTER_MOTION_HINT_MASK)
        self._current_stroke_object_id = None

    def set_mode(self, mode):
	if not mode in ('stroke', 'text'):
	    print "Unknown mode %s!" % (mode,)
	    return
	self._mode = mode

    def motion_notify (self, widget, event):
	if event.is_hint:
            x, y, state = event.window.get_pointer()
	else:
            x = event.x; y = event.y
            state = event.state
	if state & gtk.gdk.BUTTON1_MASK and self._current_stroke_object_id:
            self.model.add_actions ([AppendPoints(None, self._current_stroke_object_id, "stroke", [Point (x, y)])])
        return False

    def button_release (self, widget, event):
	if self._mode == 'stroke':
            self.model.commit_transaction()
            self._current_stroke_object_id = None
	    #self.model.end_stroke (True)
            #action = Create(None, "stroke", {'d':self._current_strokes_points})
            #self.model.add_actions ([action])
            #self._current_strokes_points = None

    def button_press (self, widget, event):
	x = event.x; y = event.y
	if self._mode == 'stroke':
            self.model.begin_transaction()
            action = Create(None, "stroke", {'d':[Point(x,y)]})
            self.model.add_actions ([action])
            self._current_stroke_object_id = action.get_object_id()
	elif self._mode == 'text':
	    dlg = gtk.Dialog()
	    dlg.add_buttons(gtk.STOCK_OK, gtk.RESPONSE_ACCEPT,
			    gtk.STOCK_CANCEL, gtk.RESPONSE_REJECT)
	    dlg.set_default_response(gtk.RESPONSE_ACCEPT)
	    input = gtk.Entry()
	    input.show()
	    dlg.vbox.add(input)
	    if dlg.run() == gtk.RESPONSE_ACCEPT:
                self.model.add_actions ([Create(None, "text", {'x':x, 'y':y, 'text':input.get_text()})])
	    else:
		print "cancelled"
            dlg.destroy()

    def draw_stroke (self, ctx, stroke):
        points = stroke.get_points()
        if points == []:
            return
        point = points[0]
        ctx.move_to (points[0].get_x(), points[0].get_y())
        for point in points[1:]:
            ctx.line_to(point.get_x(), point.get_y())
        else:
            # Kinda ugly, but...
            ctx.line_to(point.get_x() + 1, point.get_y() + 1)
            
        ctx.stroke ()

    def expose_event (self, widget, event):
        #Set up the cairo context
        drawable = self.window
        width = self.allocation.width
        height = self.allocation.height

        drawable.clear()

        ctx = cairo.Context()
        cairo.gtk.set_target_drawable(ctx, drawable)
        ctx.set_rgb_color(0, 0, 0)
	ctx.select_font("Sans", cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_BOLD)
	ctx.scale_font(20.0)

        objects = self.model.get_objects ()
        for object in objects.values():
            if isinstance (object, Stroke):
                self.draw_stroke (ctx, object)
            elif isinstance (object, Text):
                ctx.move_to (object.get_x (), object.get_y ())
                ctx.show_text (object.get_text ())                
            else:
                print ("WARNING: unknown object '%s'" % (object))
                #raise NotImplementedError ("unknown object type while drawing to display")

    def model_changed (self, model, changset):
        self.queue_draw ()


class MainWindow:
    def __init__ (self, dispatcher):
        if WORKING_LOCALLY:
            basedir = './'
        else:
            basedir = DATADIR
        self.xml = gtk.glade.XML (os.path.join(basedir, 'whiteboard.glade'))
        self.xml.get_widget ('draw_image').set_from_file (os.path.join(basedir,'pen.png'))
	self.xml.get_widget ('text_image').set_from_file (os.path.join(basedir, 'text.png'))
        self.xml.get_widget ('erase_image').set_from_file (os.path.join(basedir, 'eraser.png'))
        self.xml.get_widget ('circle_image').set_from_file (os.path.join(basedir, 'circle.png'))
        self.model = Model (dispatcher)
        self.display = Display (self.model)
        self.display.set_size_request (400, 400)
        self.display.show ()
        self.xml.get_widget ('whiteboard_frame').add (self.display)
        window = self.xml.get_widget ('whiteboard_window')
        window.set_title (window.get_title () + " - " + WHITEBOARD_VERSION)
        window.connect ('delete_event', gtk.main_quit)
	self.xml.get_widget ('draw_radio').connect("toggled", self._mode_change, self.display, 'stroke')
	self.xml.get_widget ('text_radio').connect("toggled", self._mode_change, self.display, 'text')

    def _mode_change(self, btn, display, name):
	print "changing mode: %s" % (name,)
	display.set_mode(name)

def main():
    m = MainWindow (None)
    gtk.main ()

if __name__ == '__main__':
    main()
