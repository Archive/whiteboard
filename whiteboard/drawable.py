import random, time
import string
import UserDict
import xml.sax.saxutils

# Stupid hack for allowing static class members
class Callable:
    def __init__(self, anycallable):
        self.__call__ = anycallable
        
class DrawableRegistry(UserDict.UserDict):
    def register(self, name, attributes, constructor):
	attrmap = {}
	for elt in attributes:
	    attrmap[elt] = 42
	self.data[name] = (attrmap, constructor)
	self._gen_uuid_fn = self._stupid_gen_uuid

    def _stupid_gen_uuid(self):
	return "%s-%s" % tuple(map(str,(time.time(), random.random())))

    def set_uuid_function(self, fn):
	self._gen_uuid_fn = fn

    def gen_uuid(self):
	return self._gen_uuid_fn()

class Drawable:
    def __init__(self, name):
	self._name = name
	self._uid = drawable_registry.gen_uuid()

    # Implement __deepcopy__ for subclasses, see xchat/README item 3b.
    def deepcopy_from (self, old, memo):
        self._name = old._name
        self._uid = old._uid
        
    def get_uid(self):
	return self._uid

    def _xmlquote(self, msg):
	return xml.sax.saxutils.escape(msg)

    def _msg_split(self, form, attrs):
	quoted_name = self._xmlquote(self._name) 
	attr_string  = self._hash_xml(attrs)
	return "<%s uuid=\"%s\">%s</%s>" % (form,
					    self._xmlquote(self._uid),
					    "<%s %s/>" % (quoted_name, attr_string),
					    form)

    def get_attributes(self):
	raise Exception("must implement set_attributes")

    def load_attributes(self, attrs):
	raise Exception("must implement load_attributes")

    def _hash_xml(self, hash):
	fragments=[]
	for (key, value) in hash.iteritems():
	    fragments.append("%s=\"%s\"" % (self._xmlquote(key), self._xmlquote(value)))
	return string.join(fragments, ' ')

    def create_msg(self):
	return self._msg_split("create", self.get_attributes())

    def modify_msg(self, attribs):
	if attribs is None:
	    attribs = self.get_attributes()
	return self._msg_split("modify", attribs)

    def delete_msg(self, attribs):
	return self._msg_split("delete", {})

drawable_registry = DrawableRegistry()    
