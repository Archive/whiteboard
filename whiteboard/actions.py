from whiteboard.stroke import Stroke, Point
from whiteboard.text import Text
from whiteboard.drawable import drawable_registry

def _serialize_attributes(object_class, attributes):
    attribute_strings = [ ]
    for attribute_name in attributes.keys():
        attribute_strings.append('%s="%s"' % (attribute_name,
                                              object_class.serialize_attribute(attribute_name, attributes[attribute_name])))
        
    return " ".join(attribute_strings)

def _serialize_points(points):
    serialized_points = [ ]
    for point in points:
        serialized_points.append("M " + point.serialize())
    points_string = " ".join(serialized_points)
    return points_string

class Action:
    def __init__(self, request_id, object_id, object_type):
        self._request_id = request_id
        self._object_id = object_id
        self._object_type = object_type

	(object_attributes, object_class) = drawable_registry[self._object_type]
        self._object_class = object_class

    def run(self, objects):
        """Modifies the dict 'objects' based on the current action
        Returns a list of objects that were changed."""
        raise NotImplementedError("must override")

    def serialize(self):
        """Returns protocol defined XML form for the Action"""
        raise NotImplementedError("must override")

    def get_object_id(self):
        return self._object_id

    def get_request_id(self):
        return self._request_id

    def get_object_type(self):
        return self._object_type

    def get_object_class(self):
        return self._object_class

    def set_request_id(self, request_id):
        self._request_id = request_id

    def set_object_id(self, object_id):
        self._object_id = object_id
    
class Create(Action):
    def __init__(self, request_id, object_type, attributes):
        Action.__init__(self, request_id, request_id, object_type)
        self._object_type = object_type
        self._attributes = attributes

    def set_request_id(self, request_id):
        Action.set_request_id(self, request_id)
        self.set_object_id(request_id)

    def run(self, objects):
        object_instance = self._object_class()
	object_instance.load_attributes(self._attributes)
        objects[self.get_object_id()] = object_instance
        return [object_instance]

    def serialize(self):
        attributes = _serialize_attributes(self.get_object_class(), self._attributes)
        return '<%s action="create" requestId="%s" %s/>' % (self.get_object_type(),
                                                           self.get_request_id(),
                                                           attributes)
                                                           
class Modify(Action):
    def __init__(self, request_id, object_id, object_type, attributes):
        Action.__init__(self, request_id, object_id, object_type)
        self._attributes = attributes

    def run(self, objects):
        object_instance = objects[self.get_object_id()]
        object_instance.load_attributes(self._attributes)
        return [object_instance]

    def serialize(self):
        attributes = _serialize_attributes(self.get_object_class(), self._attributes)
        return '<%s action="modify" requestId="%s" objectId="%s" %s/>' % (self.get_object_type(),
                                                                         self.get_request_id(),
                                                                         self.get_object_id(),
                                                                         attributes)        

class SubtractPoints(Action):
    def __init__(self, request_id, object_id, object_type, points):
        Action.__init__(self, request_id, object_id, object_type)
        self._points = points

    def run(self, objects):
        object_instance = objects[self.get_object_id()]
        for point in self._points:
            object_instance.remove_point(point)
        return [object_instance]

    def serialize(self):
        return '<%s action="subtract" requestId="%s" objectId="%s" d="%s"/>' % (self.get_object_type(),
                                                                            self.get_request_id(),
                                                                            self.get_object_id(),
                                                                            _serialize_points(self._points))
class AppendPoints(Action):
    def __init__(self, request_id, object_id, object_type, points):
        Action.__init__(self, request_id, object_id, object_type)
        self._points = points

    def append_more_points(self, more_points):
        """Adds a few more points to the point list of the action...used
        by the action stream compression in model.py"""
        for extra_point in more_points:
            self._points.append(extra_point)

    def get_points(self):
        return self._points

    def run(self, objects):
        object_instance = objects[self.get_object_id()]
        for point in self._points:
            object_instance.append_point(point)
        return [object_instance]

    def serialize(self):
        return '<%s action="append" requestId="%s" objectId="%s" d="%s"/>' % (self.get_object_type(),
                                                                              self.get_request_id(),
                                                                              self.get_object_id(),
                                                                              _serialize_points(self._points))    

class Delete(Action):
    def __init__(self, request_id, object_id, object_type):
        Action.__init__(self, request_id, object_id, object_type)

    def run(self, objects):
        return [objects.pop(self.get_object_id())]

    def serialize(self):
        return '<%s action="delete" requestId="%s" objectId="%s"/>' % (self.get_object_type(),
                                                                      self.get_request_id(),
                                                                      self.get_object_id())

class ActionFactory:
    def create(self, object_type, attributes):
        action = attributes.pop('action')
        request_id = attributes.pop('requestId')

        if attributes.has_key('objectId'):
            object_id = attributes.pop('objectId')

        # FIXME, store and use
        if attributes.has_key('serial'):
            serial = attributes.pop('serial')

        (valid_attrs, object_class) = drawable_registry[object_type]
        
        if action == 'create':
            action = Create(request_id, object_type, self._deserialize(object_class, attributes))
        elif action == 'modify':
            action = Modify(request_id, object_id, object_type, self._deserialize(object_class, attributes))
        elif action == 'delete':
            action = Delete(request_id, object_id, object_type)
        elif action == 'append':
            action = AppendPoints(request_id, object_id, object_type, self._get_points(object_class, attributes))
        else:
            raise NotImplementedError("Stream contained unimplemented action '%s'" % (action))

        return action

    def _deserialize(self, object_class, raw_attributes):
        attributes = { }
        for attribute_name in raw_attributes.keys():
            attributes[attribute_name] = object_class.deserialize_attribute(attribute_name, raw_attributes[attribute_name])
            assert(attributes[attribute_name] != None)
        return attributes
                                                                           
    def _get_points(self, object_class, raw_attributes):
        attributes = self._deserialize(object_class, raw_attributes)
        return attributes['d']


action_factory = ActionFactory()

def get_test_actions():
    actions = []
    point_list = [Point(5, 5, 5), Point(10, 10, 10), Point(5, 10, 15)]
    
    actions.append(Create('request9', 'text', {'x':5, 'y':10, 'text':"hello world"}))
    actions.append(Create('request10', 'stroke', {'d':point_list}))
    actions.append(Modify('request11', 'request10', 'stroke', {'d':point_list}))
    actions.append(Delete('request12', 'request10', 'stroke'))

    return actions
    
if __name__ == '__main__':
    actions = get_test_actions()

    for action in actions:
        print action.serialize()
