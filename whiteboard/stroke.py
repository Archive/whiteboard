from whiteboard.drawable import Drawable, Callable
import whiteboard.drawable
import string,re 
import sets
import copy

_svg_path_re = re.compile('(M\s*\d+\s+\d+(\s+\d+\s+\d+)*)|(C\s*\d+\s+\d+\s+\d+\s+\d+\s+\d+\s+\d+(\s+\d+\s+\d+\s+\d+\s+\d+\s+\d+\s+\d+)*)')


def _compute_svg_path(points):
    ret = []
    for point in points:
        ret.append('M %s' % (point.serialize()))
    return string.join(ret, ' ')

def _read_svg_path(path):
    points = [ ]
    cmds = _svg_path_re.findall(path)
    for cmd in filter(lambda x: not (x is None or x == ''), cmds):
        #WTF
        cmd = cmd[0]
        digits = map(int, filter(lambda x: not x == '', cmd[1:].split(' ')))
        if cmd[0] == 'M':
            while digits != []:
                points.append(Point(digits[0], digits[1]))
                digits = digits[2:]
        elif cmd[0] == 'C':
            while digits != []:
                digits = digits[6:]

    return points



class Point:
    def __init__(self, x, y, width=2):
        self._x = x
        self._y = y
        self._width = width

    # Implement __deepcopy__, see xchat/README item 3b.
    # also provides an optimzation since points are immutable
    def __deepcopy__(self,memo):
        return self                     # immutable

    def get_x(self):
        return self._x

    def get_y(self):
        return self._y

    def serialize(self):
        return "%d %d" % (self._x, self._y)
    
    def __str__(self):
        return "(%d,%d)" % (self._x, self._y)

class Stroke(Drawable):
    def __init__(self, points = None):
	Drawable.__init__(self, "stroke")
        if not points:
            self._points = []
        else:
            self._points = points

    # Implement __deepcopy__, see xchat/README item 3b.
    def __deepcopy__(self, memo):
        new = Stroke()
        new.deepcopy_from(self, memo)
        return new
        
    def deepcopy_from (self, old, memo):
        Drawable.deepcopy_from(self, old, memo)
        self._points = copy.deepcopy (old._points, memo=memo)
        
    def get_points(self):
        return self._points

    def get_attributes(self):
	return {'d': _compute_svg_path(self.get_points())}

    def serialize_attribute(attr_name, attr_value):
        assert (attr_name in ['d'])
        return  _compute_svg_path(attr_value)

    def deserialize_attribute(attr_name, attr_value):
        assert (attr_name in ['d'])
        return _read_svg_path(attr_value)
    
    # Stupid hack to allow static "class method"
    serialize_attribute = Callable(serialize_attribute)
    deserialize_attribute = Callable(deserialize_attribute)
    
    def load_attributes(self, attrs):
	for (key,value) in attrs.iteritems():
	    if key == 'd':
                if value:
                    for point in value:
                        self.append_point(point)

    # Drawing
    def append_point(self, point):
        self._points.append(point)


    # Erasing
    def erase_point(self, point):
        pass

whiteboard.drawable.drawable_registry.register('stroke', sets.Set(['d']), Stroke)

if __name__ == '__main__':
    t = Stroke()
    print t.create_msg()
    t.append_point(Point(10, 20))
    print t.create_msg()
    t = Stroke()
    t.load_attributes({'d': 'M 10 10'})
    print t.create_msg()

    
