# -*- mode: python; coding: utf-8 -*-
# Copyright © 2004 Colin Walters <walters@verbum.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import xml.parsers.expat
import xml.sax
from xml.sax.handler import ContentHandler
from xml.sax.xmlreader import IncrementalParser

import gobject
from whiteboard.drawable import drawable_registry
from whiteboard.actions import action_factory


class ProtocolHandler(ContentHandler):
    def __init__(self, dispatcher):
	self._dispatcher = dispatcher
	self._state = 'start'

    def startElement(self, name, attrs):
        if name == 'request' and self._state == 'start':
            self._authoritative = False
            self._state = 'action'            
        elif name == 'exec' and self._state == 'start':
            self._authoritative = True
            self._state = 'action'
        elif drawable_registry.has_key(name) and self._state == 'action':
            myattrs = { }
            for x in attrs.getNames():
                myattrs[x] = attrs[x]

            action = action_factory.create(name, myattrs)
            self._dispatcher.add_actions([action], self._authoritative)
            
            self._state = 'end'
	else:
            raise Exception("Invalid state reached in ProtocolHandler")

    def endElement(self, name):
        if name == 'request' or name == 'exec':
            if self._state == 'end':
                self._dispatcher.request_complete()
            else:
                raise Exception("Request ended before parsing was complete")

class Dispatcher(gobject.GObject):
    __gsignals__ = {
        'add_actions' : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, (gobject.TYPE_PYOBJECT,gobject.TYPE_BOOLEAN))
        }

    def __init__(self, transport):
        gobject.GObject.__init__(self)
	self._transport = transport
	self._parser = None
	self._transport.connect("read", self._read_tp)
	self._inbuf = ''

    # this is virtualized to accomodate a hack in xchat_wb where xml.sax.make_parser()
    # doesn't work from the GUI interpreter
    def make_parser(self):
        return xml.sax.make_parser()

    def _read_tp(self, tp, data):
	if self._parser is None:
	    self._parser = self.make_parser()
	    self._parser.setContentHandler(ProtocolHandler(self))
	self._parser.feed(data)

    def request_complete(self):
        self._parser = None

    def add_actions(self, actions, authoritative):
        self.emit("add_actions", actions, authoritative)
        
    def dispatch_actions(self, actions):
        """Transmit a list of actions to the master client"""
	for action in actions:
            serialized_action = action.serialize()
            self._transport.write("<request>%s</request>" % (serialized_action))

    def get_initialization_actions(self):
	return []

gobject.type_register(Dispatcher)

def _test_add_actions_cb(dispatcher, actions, authoritative):
    print "Received (authoritative=" + str(authoritative) + "):"
    for action in actions:
        print "\t%s" % (action.serialize())

def get_test_dispatcher():
    from whiteboard.transports.echo_transport import EchoTransport
    
    transport = EchoTransport()
    return Dispatcher(transport)

if __name__ == '__main__':
    dispatcher = get_test_dispatcher()
    dispatcher.connect("add_actions", _test_add_actions_cb)

    from whiteboard.actions import get_test_actions
    actions = get_test_actions()

    dispatcher.dispatch_actions(actions)
