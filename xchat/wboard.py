"""
Authors: Chris Blizzard, Bryan Clark, Owen Taylor
"""

__module_name__ = "whiteboard" 
__module_version__ = "0.1" 
__module_description__ = "Shared Whiteboard Module for XChat"

import xchat

def wb_cb(word, word_eol, userdata):
    import whiteboard.xchat_wb

    return whiteboard.xchat_wb.doCommand (word, word_eol, userdata)
    
    return xchat.EAT_ALL 

xchat.hook_command("WB", wb_cb, help="/WB use to start a white board session")
xchat.prnt("Shared whiteboad loaded")
