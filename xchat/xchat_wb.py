import re
import thread
import threading
import xml.sax

import gobject
import xchat

import whiteboard.irc_client_parser
import whiteboard.transports.transport

from whiteboard.display import MainWindow
import whiteboard.dispatcher

#
# We need to call gobject.threads_init() when threading is on in
# in the interpreter, or callbacks won't properly acquire the
# global interpreter lock
#
gobject.threads_init()

transports = {}

# XML parser created in advance for the main thread, which
# isn't able to create one (see 3a in xchat/README)
spare_xml_parser = None

def debug_print(message):
    xchat.prnt("\002" + message + "\003\n")

class XChatTransport(whiteboard.transports.transport.Transport):
    def __init__(self, server, chname):
        
        whiteboard.transports.transport.Transport.__init__(self)
        self.server = server
        self.chname = chname
	self.parser = whiteboard.irc_client_parser.ClientParser()
        self.master = None

        self.queue_cv = threading.Condition()
        self.queue = []
        thread.start_new_thread(self._execQueue, ())

    def sendMessage(self, to, message):
        context = xchat.find_context(server=self.server, channel=self.chname)
        if context:
            context.command("msg %s %s" % (to, message))
            
    def sendEncoded (self, command, to=None):
        # IRC message length starts with a 512 buffer
        #
        # - 2 for CRLF
        # - len(":$prefix privmsg ")           10 + len($prefix) 
        # - len("$receiver :")                 2 + len($receiver)
        # - len("WHITEBOARD $channel$index+ ") 23 + len($channel)
        #
        # Gives the length of the payload

        username = xchat.get_prefs("irc_user_name")
        nick = xchat.get_info("nick")

        # Prefix is nick!username@host. Very hard to figure out
        # what host xchat is using...
        prefixlen = len(nick) + len(username) + 128 + 2

        if to:
            channelStr = self.chname + " "
        else:
            channelStr = ""
            to = self.chname

        maxLength = 473 - prefixlen - len(to) - len(channelStr)
        
        length = len(command)
        offset = 0
        index = 0
        while (offset + maxLength < length):
            str = ("WHITEBOARD %s%d+ " % (channelStr, index)) + command[offset:offset+maxLength]
            self.sendMessage(to, str)
            offset += maxLength
            index += 1
            
        str = ("WHITEBOARD %s%d " % (channelStr, index)) + command[offset:]
        self.sendMessage (to, str)

    def doWrite(self, data, private):
        if private and self.master:
            to = self.master
        else:
            to = None
        self.sendEncoded(data, to)

    #
    # We define a message queue with a helper thread to get around the fac
    # that our user interface is operating in the main interpreter, but
    # xchat operations need to run in the plugin interpreter. The worker thread is
    # always running in the plugin interpreter, so we add messages we want to
    # write to that queue and let it execute them
    #
    def _execQueue(self):
        while True:
            self.queue_cv.acquire()
            while (len(self.queue) == 0):
                self.queue_cv.wait()
            tmp = self.queue
            self.queue = []
            self.queue_cv.release()
                
            for item in tmp:
                self.doWrite(item[0], item[1])

    def write(self, data, private=False):
        self.queue_cv.acquire()
        self.queue.append((data,private))
        self.queue_cv.notify()
        self.queue_cv.release()

    #
    # Getting reads from this thread to the GUI interpreter is easier, we
    # just use an idle. (Passing a parser along with the idle in case it is needed)
    #
    def _emitRead(self, message, parser):
        global spare_xml_parser
        spare_xml_parser = parser
        self.emit('read', message)
        spare_xml_parser = None

    def processMessage(self, message):
        gobject.idle_add(self._emitRead,
                         message, xml.sax.make_parser(),
                         priority=gobject.PRIORITY_HIGH)

    def updateTopic(self, newtopic):
        self.master = None
        if newtopic:
            m = re.search("(^|\s)master=(\S+)(?=$|\s)", newtopic)
            if m:
                self.master = m.group(2)
        print "Master is", self.master

# Subclass to accomodaae the 'spare_xml_parser' hack described above
class XChatDispatcher(whiteboard.dispatcher.Dispatcher):
    def make_parser(self):
        global spare_xml_parser
        assert spare_xml_parser
        parser = spare_xml_parser
        spare_xml_parser = None
        return parser

def privmsg_cb(word, word_eol, userdata):
    nick = word[0].split('!')[0][1:]
    message = word_eol[3][1:]

    if re.match("^WHITEBOARD (([^0-9 ]+) )?([0-9]+\+? .*)", message):
        # Need a parser to extract the channel name, but we don't care which
        try:
            parser = transports.values()[0].parser
        except IndexError:
            return xchat.EAT_NONE
        
        try:
            chname = parser.get_channel(word[2], message)
        except whiteboard.irc_client_parser.ClientProtocolError, e:
            debug_print(e.value)
            return xchat.EAT_NONE
        
        server = xchat.get_info("server")
        
        if not (server,chname) in transports:
            return xchat.EAT_NONE

        transport = transports[(server,chname)]
	parser = transport.parser

        try:
            message = parser.parse_message(nick, message)
        except whiteboard.irc_client_parser.ClientProtocolError, e:
            debug_print("Protocol error for nick %s: %s" % (nick, e.value))
            parser.end_message(nick)
            return xchat.EAT_NONE

        transport.processMessage(message)

        return xchat.EAT_ALL

    return xchat.EAT_NONE

def doCommand(word, word_eol, userdata):
    def usage():
        xchat.prnt("Usage: /wb [join] <channel>")
        return xchat.EAT_ALL

    if len(word) < 2:
        return usage()
    elif len(word) == 2:
        chname = word[1]
    elif word[1] == 'join':
        if len(word) != 3:
            return usage()
        chname = word[2]
    else:
        return usage()

    if not whiteboard.irc_client_parser.isChannel(chname):
        return usage()
    
    server = xchat.get_info("server")

    if (server,chname) in transports:
        xchat.prnt("Already joined to whiteboard in %s" % chname)
        return xchat.EAT_ALL

    transport = XChatTransport(server, chname)
    transports[(server,chname)] = transport

    context = xchat.find_context(server=server, channel=chname)
    if context:
        transport.updateTopic (context.get_info("topic"))
    else:
        context = xchat.find_context(server=server)
        context.command ("join %s" % chname)

    dp = XChatDispatcher(transport)
    m = MainWindow (dp)
    
    return xchat.EAT_ALL 

# Format of topic command is
# :prefix topic :<topic>

def topic_cb(word, word_eol, userdata):
    chname = word[2]
    topic = word_eol[3][1:]
    server = xchat.get_info("server")
    if (server,chname) in transports:
        transports[(server,chname)].updateTopic(topic)
    return xchat.EAT_NONE

# We get a namelist after joining channel, this is a convenient
# place to update the topic; xchat will already have received the
# topic (if any) and updated its internal state
def rpl_endofnames_cb(word, word_eol, userdata):
    chname = word[3]
    server = xchat.get_info("server")
    context = xchat.find_context(server=server, channel=chname)
    if context:
        topic = context.get_info("topic")
        if (server,chname) in transports:
            transports[(server,chname)].updateTopic(topic)
    return xchat.EAT_NONE

def your_message_cb(word, word_eol, userdata):
    if re.match("^WHITEBOARD (([^0-9 ]+) )?([0-9]+\+? .*)", word_eol[1]):
        return xchat.EAT_ALL
    else:
        return xchat.EAT_NONE

xchat.hook_server("PRIVMSG", privmsg_cb)
xchat.hook_server("TOPIC", topic_cb)
xchat.hook_server("366", rpl_endofnames_cb)
xchat.hook_print("Your Message", your_message_cb)
