#!/usr/bin/python

###
# WBoard plugin for supybot
#
# Copyright (C) 2004 Red Hat, Inc
#
# The WBoard plugin is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# Nautilus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#
# Authors: Owen Taylor <otaylor@redhat.com>
###

"""
The WBoard plugin implements the master for a shared whiteboard
"""

import plugins

import conf
import ircdb
import ircmsgs
import ircutils
import re
import utils
import privmsgs
import callbacks

import xml.sax
import xml.sax.handler
from xml.sax.saxutils import quoteattr

import whiteboard.irc_client_parser

def configure(advanced):
    # This will be called by setup.py to configure this module.  Advanced is
    # a bool that specifies whether the user identified himself as an advanced
    # user or not.  You should effect your configuration by manipulating the
    # registry as appropriate.
    from questions import expect, anything, something, yn
    conf.registerPlugin('Whiteboard', True)

class BadCommand(Exception):
    pass

ObjectTypes = {
    'stroke': ('d'),
    'text': ('x','y','text')
}

class Request:
    def __init__(self, element, attrs):
        if not element in ObjectTypes:
            raise BadCommand("Unknown request element: <%s>" % element)
        self.element = element
        self.requestId = None
        self.objectId = None
        self.serial = None
        self.action = None
        self.attrs = {}        
        for key in attrs.getNames():
            val = attrs.getValue(key)
            if key == 'requestId':
                self.requestId = val
            elif key == 'objectId':
                self.objectId = val
            elif key == 'serial':
                self.serial = val
            elif key == 'action':
                if not val in ( 'create', 'modify', 'append', 'subtract', 'delete' ):
                    raise BadCommand("Unknown action: %s" % val)
                self.action = val
            else:
                possible = ObjectTypes[element]
                if not key in possible:
                    raise BadCommand("Unknown attribute '%s' for <%s>" % (key, element))
                self.attrs[key] = val

        if self.action == None:
            raise BadCommand("action must be specified for all requests")
        if not self.requestId:
            raise BadCommand("requestId must be specified for all requests")
        if self.action in ('modify', 'append', 'subtract', 'delete') and not self.objectId:
            raise BadCommand("objectId must be specified for action '%s'", self.action)
        if self.serial:
            raise BadCommand("Serial cannot be specified in a request")
            
class ChannelParser(xml.sax.handler.ContentHandler):
    def parseCommand(self, command):
        self.state = 'outside'
        self.result = None
        try:
            xml.sax.parseString (command, self)
            result = self.result
        finally:
            self.result = None

        return result

    def startElement(self, name, attrs):
        if self.state == 'outside':
            if name == 'request':
                self.state = 'request'
            elif name == 'replay':
                if attrs.has_key('serial'):
                    serial = attrs.getValue('serial')
                    if not re.match("^[0-9]+$", serial):
                        raise BadCommand("replay serial must be an integer")
                    self.result = ('replay', int(attrs.getValue('serial')))
                else:
                    raise BadCommand("<replay> must include serial attribute")
                self.state = 'replay'
            else:
                raise BadCommand("Unexpected element <%s>" % name)
        elif self.state == 'request':
            self.result = ('request', Request(name, attrs))
            self.state = 'requestinside'
        elif self.state == 'requestinside':
            raise BadCommand("requests cannot have child elements")
        elif self.state == 'requestdone':
            raise BadCommand("<request> must have exactly one child element")
        elif self.state == 'replay':
            raise BadCommand("<replay> can't have child elements")
        elif self.state == 'done':
            raise BadCommand("Must have a single toplevel element")
    
    def endElement(self, name):
        if self.state == 'request':
            raise BadCommand("<request> must have exactly one child element")
        elif self.state == 'requestinside':
            self.state = 'requestdone'
        elif self.state == 'requestdone':
            self.state = 'done'
        elif self.state == 'replay':
            self.state = 'done'

    def characters(self, content):
        if not re.match("^[ \t]*$", content):
            raise BadCommand("Unexpected text in command")

class Channel:
    def __init__(self, wboard, name):
        self.parser = whiteboard.irc_client_parser.ClientParser()
        self.name = name
        self.log = wboard.log
        self.commandParser = ChannelParser()
        self.requests = []
        self.liveObjects = {}
        self.usedIds = {}
        self.serial = 0
        self.sentStart = False

    def parse(self, irc, nick, text):
        res = None
        try:
            res = self.parser.parse_message(nick, text)
        except whiteboard.irc_client_parser.ClientProtocolError, e:
            self.log.info("From '%s'; parse error %s; resetting" % (nick, e))
            self.parser.end_message(msg.nick)
        if res:
            try:
                (command, data) = self.commandParser.parseCommand(res)
                if command == 'request':
                    self.processRequest(irc, nick, data)
                elif command == 'replay':
                    self.processReplay(irc, nick, data)
            except BadCommand, e:
                irc.error("%s" % e.args[0])
            except xml.sax.SAXParseException, e:
                irc.error("%s" % e.args[0])

    def checkTopic(self, irc):
        curtopic = irc.state.channels[self.name].topic

        # We have to be careful not to try and change the topic
        # if we aren't an op, since that confuses supybot
        if not irc.nick in irc.state.channels[self.name].ops:
            return

        if not re.search("(^|\s)master=%s(?=$|\s)" % irc.nick, curtopic) or \
           re.search("(^|\s)master=(?!%s($|\s))" % irc.nick, curtopic):
            newtopic = re.sub("(^|\s)master=\S*(?=$|\s)", "", curtopic)
            if newtopic != "":
                newtopic = ("master=%s " % irc.nick) + newtopic
            else:
                newtopic = "master=%s" % irc.nick
            irc.queueMsg(ircmsgs.topic(self.name, newtopic))

    def checkStart(self, irc):
        if not self.sentStart:
            self.sendEncoded (irc, "<start/>")
            self.sentStart = True
            
    def removeTopic(self, irc):
        curtopic = irc.state.channels[self.name].topic

        # We have to be careful not to try and change the topic
        # if we aren't an op, since that confuses supybot
        if not irc.nick in irc.state.channels[self.name].ops:
            return
        
        if re.search("(^|\s)master=%s(?=$|\s)" % irc.nick, curtopic):
            newtopic = re.sub("(^|\s)master=%s(?=$|\s)" % irc.nick, "", curtopic)
            irc.queueMsg(ircmsgs.topic(self.name, newtopic))
            
    def sendEncoded (self, irc, unicodeCommand, to=None):
        # FIXME: This isn't right since we might split characters;
        # we might just want to escape all non-ASCII
        command = unicodeCommand.encode("UTF-8")
        
        # Not sure how accurate this is, but its the same that
        # supybot uses elsewhere. We allow 23 characters
        # for 'WHITEBOARD <digits>+ '.
        maxLength = 427 - len(irc.prefix)
        length = len(command)

        if to:
            channelStr = self.name + " "
            maxLength -= len(channelStr)
        else:
            channelStr = ""
            to = self.name
        
        offset = 0
        index = 0
        while (offset + maxLength < length):
            str = ("WHITEBOARD %s%d+ " % (channelStr, index)) + command[offset:offset+maxLength]
            irc.queueMsg(ircmsgs.privmsg(to, str))
            offset += maxLength
            index += 1
            
        str = ("WHITEBOARD %s%d " % (channelStr, index)) + command[offset:]
        irc.queueMsg(ircmsgs.privmsg(to, str))

    def encodeRequest(self, request):
        attrstr = "serial=%s requestId=%s action=%s" % (quoteattr(str(request.serial)), quoteattr(request.requestId), quoteattr(request.action))
        
        if request.action != 'create':
            attrstr += " objectId=%s" % quoteattr(request.objectId)
            
        for key, val in request.attrs.iteritems():
            attrstr += " %s=%s" % (key, quoteattr(val))

        return "<%s %s/>" % (request.element, attrstr)

    def echoRequest(self, irc, nick, request):
        request = self.encodeRequest(request)
        self.sendEncoded (irc, "<echo>"+request+"</echo>", to=nick)

    def execRequest(self, irc, nick, request):
        request = self.encodeRequest(request)
        self.sendEncoded (irc, "<exec>"+request+"</exec>")

    def failRequest(self, irc, nick, request):
        attrstr = "requestId=%s" % quoteattr(request.requestId)
        
        command = "<fail><%s %s/></fail>" % (request.element, attrstr)
        self.sendEncoded (irc, command)
        
    def processRequest(self, irc, nick, request):
        if request.requestId in self.usedIds:
            irc.error("Duplicate request ID '%s'" % request.requestId)
            return
        
        self.usedIds[request.requestId] = 1
        
        if request.action != 'create':
            if not request.objectId in self.liveObjects:
                self.failRequest(irc, nick, request)
                return

        if request.action == 'create':
            self.liveObjects[request.requestId] = 1
        elif request.action == 'delete':
            del self.liveObjects[request.objectId]

        request.serial = self.serial
        self.serial += 1
        self.requests.append(request)

        self.execRequest(irc, nick, request)

    def processReplay(self, irc, nick, serial):
        elided = {}
        for request in self.requests:
            if request.serial >= serial:
                # We can omit modify/subtract requests if we later
                # delete the object, but we can only omit a delete
                # request if we previously omitted the create request.
                #
                if request.action == 'create':
                    if not request.requestId in self.liveObjects:
                        elided[request.requestId] = 1
                        continue
                elif request.action == 'delete':
                    if request.objectId in elided:
                        continue
                else:
                    if not request.requestId in self.liveObjects:
                        continue
                    
                self.echoRequest(irc, nick, request)
    
class WBoard(callbacks.PrivmsgCommandAndRegexp):
    regexps = ['parseCommand']
    addressdRegexps = ['parseCommand']

    def __init__(self):
        self.channels = {}
        callbacks.PrivmsgCommandAndRegexp.__init__(self)

    #
    # bot commands
    #
    def start(self, irc, msg, args):
        """<channel>

        Tell the bot to start a whiteboard on channel <channel>"""

        chname = privmsgs.getArgs(args)
        if not ircutils.isChannel(chname):
            irc.error("'%s' is not a valid channel name" % chname)
            return
        if chname in self.channels:
            irc.error("Already running a whiteboard on '%s'" % chname)
            return
        self.channels[chname] = Channel(self, chname)

        if not chname in irc.state.channels:
            irc.queueMsg(ircmsgs.join(chname))
        else:
            self.channels[chname].checkTopic(irc)
            self.channels[chname].checkStart(irc)
    start = privmsgs.checkCapability(start, 'wboardadmin')
 
    def end(self, irc, msg, args):
        """<channel>

        Tell the bot to end the whiteboard currently on channel <channel>"""
        
        chname = privmsgs.getArgs(args)
        if not ircutils.isChannel(chname):
            irc.error("'%s' is not a valid channel name" % chname)
            return
        if not chname in self.channels:
            irc.error("Not running a whiteboard on '%s'" % chname)
            return

        if chname in irc.state.channels:
            self.channels[chname].removeTopic(irc)
            irc.queueMsg(ircmsgs.part(chname, "Whiteboard terminated"))

        del self.channels[chname]
    end = privmsgs.checkCapability(end, 'wboardadmin')

    #
    # Data parsing
    #
    def parseCommand(self, irc, msg, match):
        r"^(WHITEBOARD (([^0-9 ]+) )?([0-9]+\+? .*))"

        # Need a parser to extract the channel name, but we don't care which
        try:
            parser = self.channels.values()[0].parser
        except IndexError:
            return

        # Extract the channel name
        try:
            chname = parser.get_channel(msg.args[0], match.group(1))
        except whiteboard.irc_client_parser.ClientProtocolError, e:
            irc.error(e.value)
            return

        if not chname in self.channels:
            return
            
        channel = self.channels[chname]
        channel.parse(irc, msg.nick, match.group(1))
       
    #
    # Handlers to track current state
    #
    def do366(self, irc, msg): # End of /NAMES list; finished joining a channel
        chname = msg.args[1] # nick is msg.args[0].
        if chname in self.channels:
            self.channels[chname].checkTopic(irc)
            self.channels[chname].checkStart(irc)

    def doKick(self, irc, msg):
        chname = msg.args[0]
        users = msg.args[1]
        for user in users.split(','):
            if user == irc.nick:
                # self.log.info("Left channel '%s'" % chname)
                pass

    def doPart(self, irc, msg):
        if msg.nick == irc.nick:
            for chname in msg.args[0].split(','):
                # self.log.info("Left channel '%s'" % chname)
                pass
                
    def doMode(self, irc, msg):
        chname = msg.args[0]
        if chname in self.channels:
            for (mode, value) in ircutils.separateModes(msg.args[1:]):
                if mode == '+o' and value == irc.nick:
                    self.channels[chname].checkTopic(irc)
                
    def doTopic(self, irc, msg):
        chname = msg.args[0]
        topic = msg.args[1]
        if chname in self.channels:
            self.channels[chname].checkTopic(irc)

Class = WBoard
